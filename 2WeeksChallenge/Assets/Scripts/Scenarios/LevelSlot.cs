﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SuperSquad.Challenge.Common;

namespace SuperSquad.Challenge.Scenarios
{
    public class LevelSlot : Utils.MonoBehaviour
    {
        public enum SlotTypes { NORMAL, TARGET, ACTIVATED }

        [Header("Slot Type")]
        [SerializeField]
        private SlotTypes m_SlotType;
        public SlotTypes SlotType { get { return m_SlotType; } }

        [Header("Neighbours")]
        [SerializeField]
        private List<LevelSlot> m_MoveNeighbours;
        public List<LevelSlot> MoveNeighbours { get { return m_MoveNeighbours; } }
        [SerializeField]
        private List<LevelSlot> m_JumpNeighbours;
        public List<LevelSlot> JumpNeighbours { get { return m_JumpNeighbours; } }

        private MeshRenderer Renderer;

        [Header("Materials")]
        [SerializeField]
        private Material m_NormalSlotMaterial;
        [SerializeField]
        private Material m_TargetSlotMaterial;
        [SerializeField]
        private Material m_ActivatedSlotMaterial;


        // Use this for initialization
        void Awake()
        {
            Renderer = GetComponent<MeshRenderer>();

            if (Renderer != null)
                Renderer.material = m_SlotType == SlotTypes.NORMAL ? m_NormalSlotMaterial : m_TargetSlotMaterial;
        }

        public void SetAsActivated()
        {
            K.SoundManager.PlayFXSound(K.SoundManager.GetSFXByName("activate"));
            m_SlotType = SlotTypes.ACTIVATED;

            if(Renderer != null)
                Renderer.material = m_ActivatedSlotMaterial;
        }

        public void SetAsNoActivated()
        {
            m_SlotType = SlotTypes.TARGET;

            if(Renderer != null)
                Renderer.material = m_TargetSlotMaterial;
        }
    }
}

