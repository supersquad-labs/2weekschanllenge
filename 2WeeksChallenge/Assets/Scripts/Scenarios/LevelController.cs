﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SuperSquad.Challenge.Common;

namespace SuperSquad.Challenge.Scenarios
{
    public class LevelController : Utils.MonoBehaviour
    {
        [Header("Player")]
        [SerializeField]
        private Player m_Player;
        public Player Player { get { return m_Player; } }
        [Header("Start Slot")]
        [SerializeField]
        private GameObject m_StartSlot;

        [Header("Target Slots")]
        [SerializeField]
        private List<LevelSlot> m_TargetSlots;

        [Header("Level Dialogue")]
        [SerializeField]
        private string m_LevelDialogue;
        public string LevelDialogue { get { return m_LevelDialogue; } }
        public bool HasLevelDialogue { get { return string.IsNullOrEmpty(m_LevelDialogue); } }

        public bool LevelCompleted { get; private set; }


        public void Init()
        {
            m_Player.transform.parent = m_StartSlot.transform;
            m_Player.AssignToSlot(m_StartSlot);
            m_Player.StartSlot = m_StartSlot;

            if (!HasLevelDialogue)
                K.GameManager.WorldHudController.LaunchDialogue(m_LevelDialogue);

            InvokeRepeating("CheckLevelCompleted", 0, 1f);
        }

        private void CheckLevelCompleted()
        {
            List<bool> results = new List<bool>();
            for(int i= 0; i < m_TargetSlots.Count; i++)
            {
                LevelSlot slot = m_TargetSlots[i];
                if (slot.SlotType == LevelSlot.SlotTypes.ACTIVATED)
                    results.Add(true);
            }

            if (results.Count == m_TargetSlots.Count)
                LevelCompleted = true;
            else
                LevelCompleted = false;
        }

        public void ResetTargetSlots()
        {
            for(int i=0; i < m_TargetSlots.Count; i++)
            {
                m_TargetSlots[i].SetAsNoActivated();
            }
        }
    }
}

