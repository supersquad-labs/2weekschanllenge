﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using PixelCrushers.DialogueSystem;

using SuperSquad.Challenge.UI.Core.Screens;

namespace SuperSquad.Challenge.Scenarios
{
    public class ScenarioController : Utils.MonoBehaviour
    {
        [Header("Scenario Levels")]
        [SerializeField]
        private List<LevelController> m_Levels = new List<LevelController>();

        public int Index;
        public bool Completed { get; private set; }

        public LevelController CurrentLevel { get; private set; }

        LevelTransitionScreen TransitionScreen;
        bool WaitForTransition;

        private IEnumerator _CheckCurrentLevelCompleted()
        {
            while (!CurrentLevel.LevelCompleted)
                yield return null;

            if (Index == m_Levels.Count-1)
            {
                CompleteScenario();
            }
            else
            {
                yield return new WaitForSeconds(1);

                K.SoundManager.PlayFXSound(K.SoundManager.GetSFXByName("complete_level"));
                K.GameManager.WorldHudController.SetToPlayButton();
                K.GameManager.WorldHudController.EnablePlayButton(false);
                K.GameManager.WorldHudController.EnableResetButton(false);

                if (TransitionScreen == null)
                    TransitionScreen = K.ScreenManager.GetScreen<LevelTransitionScreen>("LevelTransitionScreen");

                TransitionScreen.OnScreenOpenDone = () =>
                {
                    K.GameManager.WorldHudController.ShowButtons(false);
                    ShowNextLevel();
                };

                TransitionScreen.OnScreenCloseDone = () =>
                {
                    K.GameManager.WorldHudController.ShowButtons(true);
                    WaitForTransition = false;
                };

                TransitionScreen.Init(0.5f);
                TransitionScreen.OpenScreen();

                while (WaitForTransition)
                    yield return null;
            }
        }

        public void ShowLevel(int index)
        {
            Index = index;
            m_Levels[index].gameObject.SetActive(true);
            m_Levels[index].Init();

            CurrentLevel = m_Levels[index];

            StartCoroutine(_CheckCurrentLevelCompleted());
        }

        public void ShowNextLevel()
        {
            //If the current level is not the last one, show the next one
            if(Index < m_Levels.Count-1)
            {
                if(Index > -1)
                    m_Levels[Index].gameObject.SetActive(false);

                K.SaveLoad.SaveLevelCompleted(K.GameManager.CurrentWorld, K.GameManager.CurrentLevel, 1);

                Index++;

                if(Index < m_Levels.Count)
                {
                    

                    m_Levels[Index].gameObject.SetActive(true);

                    m_Levels[Index].Init();
                    CurrentLevel = m_Levels[Index];
                    K.GameManager.CurrentLevel = Index;

                    K.GameManager.WorldHudController.SetLevelText();

                    K.GameManager.WorldHudController.ResetNotSound();
                    K.GameManager.WorldHudController.EnableResetButton(true);

                    K.SaveLoad.SaveLevelUnlocked(K.GameManager.CurrentWorld, K.GameManager.CurrentLevel, 1);

                    StartCoroutine(_CheckCurrentLevelCompleted());
                }
            }
            //else
            //{
            //    //If the current level is the last one on the list, complete the scenario
            //    CompleteScenario();
            //}
        }

        private void CompleteScenario()
        {
            K.GameManager.WorldHudController.LaunchDialogue("The Basis/Cleared");
            StartCoroutine(_CompleteScenario());
            
        }

        private IEnumerator _CompleteScenario()
        {
            K.SoundManager.PlayFXSound(K.SoundManager.GetSFXByName("win"));

            while (DialogueManager.IsConversationActive)
                yield return null;

            Completed = true;

            K.SaveLoad.SaveWorldCompleted(K.GameManager.CurrentWorld, 1);
            K.SaveLoad.SaveLevelCompleted(K.GameManager.CurrentWorld, K.GameManager.CurrentLevel, 1);

            K.GameManager.CurrentWorld++;
            K.GameManager.CurrentLevel = 0;

            K.SaveLoad.SaveWorldUnlocked(K.GameManager.CurrentWorld, 1);
            K.SaveLoad.SaveLevelUnlocked(K.GameManager.CurrentWorld, K.GameManager.CurrentLevel, 1);

            K.EnterWorldSelectionContext(); ;
        }
	}
}
