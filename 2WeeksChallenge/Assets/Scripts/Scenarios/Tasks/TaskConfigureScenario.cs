﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SuperSquad.Challenge.Scenarios.Tasks
{
    public class TaskConfigureScenario : Task.Task
    {
        [SerializeField]
        private ScenarioController m_ScenarioController;

        protected override void DoStart()
        {
            base.DoStart();

            K.GameManager.ScenarionControler = m_ScenarioController;
            m_ScenarioController.ShowLevel(K.GameManager.CurrentLevel);

            Complete("TaskConfigureScenario::Completed");
        }
    }
}

