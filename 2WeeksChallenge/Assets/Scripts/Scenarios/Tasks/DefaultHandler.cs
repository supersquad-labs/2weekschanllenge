﻿using SuperSquad.Challenge.ContextHandlers;

namespace SuperSquad.Challenge.Scenarios.Tasks
{
    public class DefaultHandler : LevelContextHandler
    {

        protected override bool CheckCanHandleContext()
        {
            if (K.CurrentContext != K.FirstWorldContext)
                return true;
            else
                return false;

        }
    }
}
