﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using SuperSquad.Challenge.Common;

namespace SuperSquad.Challenge.Actions
{
    public class ActivateAction : Action
    {
        private Player mPlayer; 

        public override void Execute()
        {
            base.Execute();
            Debug.Log("Activate");

            mPlayer = K.GameManager.Player;

            mPlayer.ActivateSlot();

            Complete("ActivateAction::Executed");
        }
    }
}

