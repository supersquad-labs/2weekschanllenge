﻿using System.Collections;
using UnityEngine;
using DG.Tweening;

using SuperSquad.Challenge.Common;
using SuperSquad.Challenge.Scenarios;

namespace SuperSquad.Challenge.Actions
{
    public class JumpAction : Action
    {
        private Player mPlayer;
        private LevelSlot mNextSlot;

        public override void Execute()
        {
            base.Execute();
            Debug.Log("Starting Jump Action");

            mPlayer = K.GameManager.Player;

            StartCoroutine(_Execute());
        }

        private IEnumerator _Execute()
        {
            mNextSlot = CheckCanJumpToSlot();

            if (mNextSlot != null)
            {
                Vector3 startPosition = mPlayer.transform.position;
                Vector3 endPosition = mNextSlot.transform.position;
                endPosition.y = startPosition.y;

                K.SoundManager.PlayFXSound(K.SoundManager.GetSFXByName("jump"));
                mPlayer.transform.DOJump(endPosition, 1, 1, 1);

                while (mPlayer.transform.position != endPosition)
                    yield return null;

                mPlayer.AssignToSlot(mNextSlot.gameObject);

                yield return new WaitForSeconds(0.5f);

                Complete("JumpAction::Executed");
            }
            else
            {
                Complete("JumpAction::Executed");
            }
        }

        private LevelSlot CheckCanJumpToSlot()
        {
            if (mPlayer.AssignedSlot.JumpNeighbours.Count > 0)
            {
                Debug.DrawRay(mPlayer.transform.position, mPlayer.transform.TransformDirection(-Vector3.forward) * 1000, Color.green);
                Vector3 fwd = mPlayer.transform.TransformDirection(Vector3.forward);

                RaycastHit hit;
                if (Physics.Raycast(mPlayer.transform.position, -fwd, out hit, 10))
                {
                    Debug.DrawRay(mPlayer.transform.position, mPlayer.transform.TransformDirection(-Vector3.forward) * 1000, Color.yellow);
                    Debug.LogWarning("Hit to jump: " + hit.transform.name);

                    if (mPlayer.AssignedSlot.JumpNeighbours.Contains(hit.transform.GetComponent<LevelSlot>()))
                    {
                        Debug.Log("Can jump to " + hit.transform.name);
                        return hit.transform.GetComponent<LevelSlot>();
                    }   
                }
                else
                {
                    Debug.LogWarning("Can't reach any slot from current slot");
                    return null;
                }
            }

            return null;
        }

        private void EnablePlayerSlotColliders(bool enable)
        {

            mPlayer.AssignedSlot.GetComponent<BoxCollider>().enabled = enable;
        }
    }
}

