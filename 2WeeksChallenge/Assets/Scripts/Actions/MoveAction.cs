﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

using SuperSquad.Challenge.Common;
using SuperSquad.Challenge.Scenarios;

namespace SuperSquad.Challenge.Actions
{
    public class MoveAction : Action
    {
        private Player mPlayer;
        private LevelSlot mNextSlot;

        public override void Execute()
        {
            base.Execute();
            Debug.Log("Starting Move Action");

            mPlayer = K.GameManager.Player;

            StartCoroutine(_Execute());
        }

        private IEnumerator _Execute()
        {
            mNextSlot = CheckSlotInFrontOfMe();

            K.SoundManager.PlayFXSound(K.SoundManager.GetSFXByName("move_rotate"));
            if (mNextSlot != null)
            {
                Vector3 startPosition = mPlayer.transform.position;
                Vector3 endPosition = mNextSlot.transform.position;
                endPosition.y = startPosition.y;



                float t = Time.deltaTime;
                while(mPlayer.transform.position != endPosition)
                {
                    mPlayer.transform.position = Vector3.Lerp(startPosition, endPosition, t);
                    t += Time.deltaTime * 2f;
                    yield return null;
                }
                    
                mPlayer.AssignToSlot(mNextSlot.gameObject);

                yield return new WaitForSeconds(0.5f);

                Complete("MoveAction::Executed");
            }
            else
            {
                Complete("MoveAction::Executed");
            }
        }

        private LevelSlot CheckSlotInFrontOfMe()
        {
            if (mPlayer.AssignedSlot.MoveNeighbours.Count > 0)
            {
                Debug.DrawRay(mPlayer.transform.position, mPlayer.transform.TransformDirection(-Vector3.forward) * 1000, Color.green);
                Vector3 fwd = mPlayer.transform.TransformDirection(Vector3.forward);

                RaycastHit hit;
                if (Physics.Raycast(mPlayer.transform.position, -fwd, out hit, 10))
                {
                    Debug.DrawRay(mPlayer.transform.position, mPlayer.transform.TransformDirection(-Vector3.forward) * 1000, Color.yellow);
                    Debug.Log("Slot target " + hit.transform.name + " is in front of me");

                    if (mPlayer.AssignedSlot.MoveNeighbours.Contains(hit.transform.GetComponent<LevelSlot>()))
                        return hit.transform.GetComponent<LevelSlot>();
                }
                else
                {
                    Debug.LogWarning("Can't reach any slot from current slot");

                    return null;
                }
            }

            return null;
        }
    }
}

