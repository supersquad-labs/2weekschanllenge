﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SuperSquad.Challenge.Actions
{
    public class Action : Utils.MonoBehaviour
    {
        public bool Completed { get; protected set; }

        public virtual void Execute() { }

        protected virtual void DoOnComplete() { }

        // Marca la tarea como completada
        public void Complete(string message)
        {
            Debug.Log(message);
            Completed = true;
            DoOnComplete();
        }

        public void NonComplete()
        {
            Completed = false;
        }
    }
}

