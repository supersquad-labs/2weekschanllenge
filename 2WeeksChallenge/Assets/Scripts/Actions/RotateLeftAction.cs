﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

using SuperSquad.Challenge.Common;

namespace SuperSquad.Challenge.Actions
{
    public class RotateLeftAction : Action
    {
        private Player mPlayer;

        public override void Execute()
        {
            base.Execute();

            Debug.Log("Starting Rotate Left Action");

            mPlayer = K.GameManager.Player;

            StartCoroutine(_Execute());

        }

        private IEnumerator _Execute()
        {
            float y = mPlayer.transform.eulerAngles.y < 0 ? 0 : mPlayer.transform.eulerAngles.y + 90f;

            Vector3 rotation = new Vector3(mPlayer.transform.eulerAngles.x, y, mPlayer.transform.eulerAngles.z);
            mPlayer.transform.DORotate(rotation, 0.5f);

            Vector3 eulerAngles = Utils.Utils.GetPositiveAngle(y);
            Debug.LogWarning("Euler Angles: " + eulerAngles);

            K.SoundManager.PlayFXSound(K.SoundManager.GetSFXByName("move_rotate"));
            while (mPlayer.transform.eulerAngles != eulerAngles)
            {
                int x = Mathf.RoundToInt(eulerAngles.y);
                if (x == 360)
                {
                    if(mPlayer.transform.eulerAngles.y < 2)
                    {
                        Debug.LogWarning("Player Euler Angles: " + mPlayer.transform.eulerAngles);
                        Complete("RotateLeftAction::Executed");
                    }

                    yield return null;
                }
                else
                {
                    Debug.LogWarning("Player Euler Angles: " + mPlayer.transform.eulerAngles);
                    yield return null;
                }
                
            }

            //if(eulerAngles.y == 0)
            //{
            //    Debug.Log(mPlayer.transform.eulerAngles.y);
            //    while (!float.IsNaN(mPlayer.transform.eulerAngles.y))
            //    {

            //        Debug.LogWarning("Player Euler Angles: " + mPlayer.transform.eulerAngles);
            //        yield return null;
            //    }
            //}
            //else
            //{
            //    while (mPlayer.transform.eulerAngles != eulerAngles)
            //    {

            //        Debug.LogWarning("Player Euler Angles: " + mPlayer.transform.eulerAngles);
            //        yield return null;
            //    }
            //}



            Complete("RotateLeftAction::Executed");
        }
    }
}

