﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

using SuperSquad.Challenge.Common;

namespace SuperSquad.Challenge.Actions
{
    public class RotateRightAction : Action
    {
        private Player mPlayer;

        public override void Execute()
        {
            base.Execute();

            Debug.Log("Starting Rotate Right Action");

            mPlayer = K.GameManager.Player;

            StartCoroutine(_Execute());

        }

        private IEnumerator _Execute()
        {
            float y = mPlayer.transform.eulerAngles.y < 0 ? 0 : mPlayer.transform.eulerAngles.y - 90f;

            Vector3 rotation = new Vector3(mPlayer.transform.eulerAngles.x, y, mPlayer.transform.eulerAngles.z);
            mPlayer.transform.DORotate(rotation, 0.5f);

            K.SoundManager.PlayFXSound(K.SoundManager.GetSFXByName("move_rotate"));
            Vector3 eulerAngles = Utils.Utils.GetPositiveAngle(y);
            while (mPlayer.transform.eulerAngles != eulerAngles)
                yield return null;

            Complete("RotateRightAction::Executed");
        }
    }
}

