﻿using UnityEngine;
using System.Collections;

using SuperSquad.Challenge.Config;

namespace SuperSquad.Challenge.Kernel.Context
{
    public class FirstWorldContext : BaseContext
    {

        public override void Enter()
        {
            Debug.Log("FirstWorldContext::Enter()");
            StartCoroutine("_WaitAndEnter");
        }

        IEnumerator _WaitAndEnter()
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(AppConfig.FIRST_WORLD_SCENE);

            yield return null;

            K.DefaultLoadingScreen.Show(false);
            SendInit();

            base.Enter();

        }

        public override void Exit()
        {
            Debug.Log("FirstWorldContext::Exit()");
            base.Exit();
        }
    }
}