﻿using UnityEngine;
using System.Collections;

namespace SuperSquad.Challenge.Kernel.Loading
{

    public interface iLoadingEventsHandler
    {

        void OnLoadingCloseAnimFinished();
        void OnLoadingOpenAnimFinished();

        float GetProgress();

    }

}