﻿using SuperSquad.Challenge.Kernel.Context;

namespace SuperSquad.Challenge.Kernel
{
    public interface iSceneBootstrap
    {

        void Init(BaseContext context);


        void InitFromKernel();
    }

}
