﻿using UnityEngine;
using System.Collections;

using SuperSquad.Challenge.Task;
using SuperSquad.Challenge.Kernel.Context;

namespace SuperSquad.Challenge.Kernel.Bootstraps
{
    public class WorldSelectionSceneBootstrap : TaskDependencyManager, iSceneBootstrap
    {

        public void Init(BaseContext context)
        {
            Debug.Log(this.Log("World Selection Scene Init"));

            StartTasks();
        }

        public void InitFromKernel()
        {
            Debug.Log(this.Log("World Selection Scene Initialized from Kernel"));
            Init(Kernel.Instance.WorldSelectionContext);
        }

        protected override void OnBootTasksCompleted()
        {
            Debug.Log(this.Log("World Selection Scene Boot Tasks Completed"));
        }
    }
}
