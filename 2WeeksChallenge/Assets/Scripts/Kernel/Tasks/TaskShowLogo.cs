﻿using UnityEngine;

using SuperSquad.Challenge.UI.Core.Screens;

namespace SuperSquad.Challenge.Kernel.Tasks
{
    public class TaskShowLogo : Task.Task
    {
        protected override void DoStart()
        {
            base.DoStart();

            BasicTransitionScreen screen = K.ScreenManager.GetScreen<BasicTransitionScreen>("Logo");
            screen.OnScreenCloseDone = () => { Complete("TaskInitSplash::Complete"); };
            screen.OpenScreen();
        }

    }
}
