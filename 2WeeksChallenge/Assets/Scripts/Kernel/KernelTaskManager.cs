﻿using System;

using SuperSquad.Challenge.Task;

namespace SuperSquad.Challenge.Kernel
{
    public class KernelTaskManager : TaskDependencyManager
    {
        // Kernel Events  
        // ------------------------------------------------
        public EventHandler KernelTasksCompleted;

        protected override void OnBootTasksCompleted()
        {
            base.OnBootTasksCompleted();
            KernelTasksCompleted(this, EventArgs.Empty);
        }
    }
}