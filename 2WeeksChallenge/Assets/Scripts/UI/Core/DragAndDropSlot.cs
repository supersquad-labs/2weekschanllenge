﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

using SuperSquad.Challenge.Utils;

namespace SuperSquad.Challenge.UI
{
    public class DragAndDropSlot : Utils.MonoBehaviour, IDropHandler
    {
        public GameObject item
        {
            get
            {
                if (transform.childCount > 0)
                {
                    return transform.GetChild(0).gameObject;
                }
                return null;
            }
        }

        #region IDropHandler implementation
        public void OnDrop(PointerEventData eventData)
        {
            if (!item)
            {
                GameObject go = ResourcesPool.Instance.GetObject(DragHandler.StartParent.name+"Image");
                go.transform.SetParent(DragHandler.StartParent);
                go.transform.position = DragHandler.StartPosition;
                go.transform.localScale = Vector3.one;
                go.GetComponent<CanvasGroup>().blocksRaycasts = true;

                transform.name = DragHandler.StartParent.name;
                DragHandler.ItemBeingDragged.transform.SetParent(transform);
                DragHandler.ItemBeingDragged.GetComponent<CanvasGroup>().blocksRaycasts = false;

            }
            else
            {
                if(item != DragHandler.ItemBeingDragged)
                {
                    item.GetComponent<CanvasGroup>().blocksRaycasts = true;
                    ResourcesPool.Instance.UnloadObject(item);

                    GameObject go = ResourcesPool.Instance.GetObject(DragHandler.ItemBeingDragged.name);
                    go.transform.SetParent(DragHandler.StartParent);
                    go.transform.localPosition = DragHandler.StartPosition;
                    go.transform.localScale = Vector3.one;
                    go.GetComponent<CanvasGroup>().blocksRaycasts = true;

                    transform.name = DragHandler.StartParent.name;
                    DragHandler.ItemBeingDragged.transform.SetParent(transform);
                    DragHandler.ItemBeingDragged.GetComponent<CanvasGroup>().blocksRaycasts = false;
                }
            }
        }
        #endregion
    }
}