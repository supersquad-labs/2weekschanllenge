﻿using UnityEngine;
using UnityEngine.EventSystems;


namespace SuperSquad.Challenge.UI
{
    public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        public static GameObject ItemBeingDragged;
        public static Vector3 StartPosition;
        public static Transform StartParent;

        #region IBeginDragHandler implementation

        public void OnBeginDrag(PointerEventData eventData)
        {
            ItemBeingDragged = gameObject;
            StartPosition = transform.position;
            StartParent = transform.parent;
            GetComponent<CanvasGroup>().blocksRaycasts = false;
        }

        #endregion

        #region IDragHandler implementation

        public void OnDrag(PointerEventData eventData)
        {
            transform.position = eventData.position;
        }

        #endregion

        #region IEndDragHandler implementation

        public void OnEndDrag(PointerEventData eventData)
        {
            Kernel.Kernel.Instance.SoundManager.PlayFXSound(Kernel.Kernel.Instance.SoundManager.GetSFXByName("click"));
            ItemBeingDragged = null;
            //GetComponent<CanvasGroup>().blocksRaycasts = true;
            if (transform.parent == StartParent)
            {
                transform.position = StartPosition;
                GetComponent<CanvasGroup>().blocksRaycasts = true;
            }
        }
        #endregion
    }
}

