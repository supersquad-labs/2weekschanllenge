﻿using System;
using System.Collections.Generic;

using SuperSquad.Challenge.ScreensManagement.BaseClaseAndUtils;

namespace SuperSquad.Challenge.UI.Core.Screens
{
    public class ScreenManagerInitializedEventArgs : EventArgs
    {
        private List<ScreenBase> mListScreens;

        public ScreenManagerInitializedEventArgs(List<ScreenBase> listScreens)
        {
            mListScreens = listScreens;
        }

        public List<ScreenBase> listScreens 
        {
            get 
                {
                return mListScreens;
            }
        }
    }
}
