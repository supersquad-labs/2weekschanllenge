﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using SuperSquad.Challenge.Actions;
using PixelCrushers.DialogueSystem;

namespace SuperSquad.Challenge.UI
{
    public class WorldHudController : Utils.MonoBehaviour
    {
        [SerializeField]
        private Button m_PlayButton;
        [SerializeField]
        private Button m_SoundButton;
        [SerializeField]
        private Button m_ResetButton;
        [SerializeField]
        private Button m_HelpButton;
        [SerializeField]
        private Button m_BackButton;

        [Space]

        [SerializeField]
        private Text m_PlayButtonText;
        [SerializeField]
        private Text m_SoundButtonText;
        [SerializeField]
        private Text m_LevelLabel;

        private DragAndDropController DragAndDropController;
        private List<Action> ActionsToExecute = new List<Action>();

        private bool SetAsRetryButton = false;
        private bool OnExecute = false;

        private void Awake()
        {
            DragAndDropController = GetComponent<DragAndDropController>();
            K.GameManager.WorldHudController = this;

            SetLevelText();
            EnablePlayButton(false);
            m_PlayButton.onClick.AddListener(() => Play());

            if(K.SoundManager.AudioSourceMuted)
            {
                m_SoundButtonText.text = "Audio On";
                m_SoundButton.onClick.RemoveAllListeners();
                m_SoundButton.onClick.AddListener(() => AudioOn());
            }
            else
            {
                m_SoundButtonText.text = "Audio Off";
                m_SoundButton.onClick.RemoveAllListeners();
                m_SoundButton.onClick.AddListener(() => AudioOff());
            }
        }

        private void Update()
        {
            if (DragAndDropController != null)
            {
                if(!OnExecute)
                {
                    if (!SetAsRetryButton)
                    {
                        if (DragAndDropController.GetActionsToExecute().Count > 0)
                            EnablePlayButton(true);
                        else
                            EnablePlayButton(false);
                    }
                }
            }
        }

        public void Play()
        {
            K.SoundManager.PlayFXSound(K.SoundManager.GetSFXByName("click"));

            EnableResetButton(false);
            DragAndDropController.EnableDropSlots(false);

            m_PlayButton.interactable = false;

            ActionsToExecute = DragAndDropController.GetActionsToExecute();
            StartCoroutine(_Play(ActionsToExecute));
        }

        public void SetToPlayButton()
        {
            m_PlayButtonText.text = "Play";
            m_PlayButton.onClick.RemoveAllListeners();
            m_PlayButton.onClick.AddListener(() => Play());

            SetAsRetryButton = false;
        }

        public void EnablePlayButton(bool enable)
        {
            m_PlayButton.interactable = enable;
        }

        public void SetToRetryButton()
        {
            m_PlayButtonText.text = "Retry";
            m_PlayButton.onClick.RemoveAllListeners();
            m_PlayButton.onClick.AddListener(() => Retry());

            SetAsRetryButton = true;
        }

        IEnumerator _Play(List<Action> actionsToExecute)
        {

            OnExecute = true;

            for (int i = 0; i < actionsToExecute.Count; i++)
            {
                actionsToExecute[i].GetComponent<Image>().color = Color.yellow;
                actionsToExecute[i].Execute();

                while (!actionsToExecute[i].Completed)
                    yield return null;

                yield return new WaitForSeconds(0.5f);
            }

            yield return new WaitForSeconds(1);

            if (!K.GameManager.ScenarionControler.CurrentLevel.LevelCompleted)
            {
                K.SoundManager.PlayFXSound(K.SoundManager.GetSFXByName("lose"));

                yield return new WaitForSeconds(1);

                SetToRetryButton();
                EnablePlayButton(true);
                EnableResetButton(true);
            }

            OnExecute = false;
        }

        public void Retry()
        {
            K.SoundManager.PlayFXSound(K.SoundManager.GetSFXByName("click"));

            SetToPlayButton();

            K.GameManager.Player.AssignToSlot(K.GameManager.Player.StartSlot);
            K.GameManager.Player.transform.eulerAngles = K.GameManager.Player.OriginalRotation;

            K.GameManager.ScenarionControler.CurrentLevel.ResetTargetSlots();

            DragAndDropController.Retry();

            for(int i=0; i < ActionsToExecute.Count; i++)
            {
                ActionsToExecute[i].GetComponent<Image>().color = Color.white;
            }

            DragAndDropController.EnableDropSlots(true);
        }

        public void Reset()
        {
            K.SoundManager.PlayFXSound(K.SoundManager.GetSFXByName("click"));

            DragAndDropController.EnableDropSlots(true);
            K.GameManager.Player.AssignToSlot(K.GameManager.Player.StartSlot);
            K.GameManager.Player.transform.eulerAngles = K.GameManager.Player.OriginalRotation;

            K.GameManager.ScenarionControler.CurrentLevel.ResetTargetSlots();

            for (int i = 0; i < ActionsToExecute.Count; i++)
            {
                ActionsToExecute[i].GetComponent<Image>().color = Color.white;
            }

            DragAndDropController.Restart();

        }

        public void HelpButton()
        {
            LaunchDialogue(K.GameManager.ScenarionControler.CurrentLevel.LevelDialogue);
        }

        public void ResetNotSound()
        {
            DragAndDropController.EnableDropSlots(true);
            K.GameManager.Player.AssignToSlot(K.GameManager.Player.StartSlot);
            K.GameManager.Player.transform.eulerAngles = K.GameManager.Player.OriginalRotation;

            K.GameManager.ScenarionControler.CurrentLevel.ResetTargetSlots();

            DragAndDropController.Restart();
        }

        public void EnableResetButton(bool enable)
        {
            m_ResetButton.interactable = enable;
        }

        public void BackToWorldSelection()
        {
            K.EnterWorldSelectionContext();
        }

        public void AudioOn()
        {
            K.SoundManager.PlayFXSound(K.SoundManager.GetSFXByName("click"));
            K.SoundManager.ResumeAudioSource();
            m_SoundButtonText.text = "Audio Off";

            K.SaveLoad.SaveCommonData(Kernel.SaveLoad.DataId.Audio, 1);

            m_SoundButton.onClick.RemoveAllListeners();
            m_SoundButton.onClick.AddListener(() => AudioOff());
        }

        public void AudioOff()
        {
            K.SoundManager.PlayFXSound(K.SoundManager.GetSFXByName("click"));
            K.SoundManager.MuteAudioSource();

            m_SoundButtonText.text = "Audio On";
            K.SaveLoad.SaveCommonData(Kernel.SaveLoad.DataId.Audio, 0);

            m_SoundButton.onClick.RemoveAllListeners();
            m_SoundButton.onClick.AddListener(() => AudioOn());
        }

        public void SetLevelText()
        {
            int currentWorld = K.GameManager.CurrentWorld + 1;
            int currentLevel = K.GameManager.CurrentLevel + 1;
            m_LevelLabel.text = "Level " + currentWorld + "-" + currentLevel;
        }

        public void LaunchDialogue(string dialogue)
        {
            StartCoroutine(_LaunchDialogue(dialogue));
        }

        private IEnumerator _LaunchDialogue(string dialogue)
        {
            DialogueManager.StartConversation(dialogue);

            while (DialogueManager.IsConversationActive)
            {
                ShowButtons(false);
                yield return null;
            }
               
            ShowButtons(true);
        }

        public void ShowButtons(bool show)
        {
            m_PlayButton.gameObject.SetActive(show);
            m_ResetButton.gameObject.SetActive(show);
            m_SoundButton.gameObject.SetActive(show);
            m_HelpButton.gameObject.SetActive(show);
            m_BackButton.gameObject.SetActive(show);
        }

    }
}

