﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace SuperSquad.Challenge.UI.Core
{
    public class WorldSelectionMenu : Utils.MonoBehaviour
    {
        [SerializeField]
        private WorldSelector m_FirstWorld;

        [Space]

        [SerializeField]
        private Button m_BackButton;

        [Space]

        [SerializeField]
        private Button m_PreviousButton;

        [Space]

        [SerializeField]
        private Button m_NextButton;

        [SerializeField]
        private Text m_WorldName;

        private WorldSelector CurrentWorld;

        private void Start()
        {
            CurrentWorld = m_FirstWorld;
            m_PreviousButton.interactable = CurrentWorld.HasPreviousWorld;
            m_WorldName.text = CurrentWorld.WorldName;
        }

        public void MainMenu()
        {
            K.SoundManager.PlayFXSound(K.SoundManager.GetSFXByName("click"));
            K.EnterMainMenuContext();
        }

        public void OnPointerEnterOnWorld()
        {
            CurrentWorld.DoShakeAnimation();
        }

        public void OnPointerClickOnWorld()
        {
            K.SoundManager.PlayFXSound(K.SoundManager.GetSFXByName("click"));
            
            if (CurrentWorld.WorldType != WorldSelector.Type.Locked && CurrentWorld.Index < 1) //TODO: just for demo
            {
                CurrentWorld.EnableLevelsCamera(true);
                m_BackButton.interactable = true;

                m_PreviousButton.interactable = false;
                m_NextButton.interactable = false;
            }

        }

        public void BackButton()
        {
            K.SoundManager.PlayFXSound(K.SoundManager.GetSFXByName("click"));
            StartCoroutine(_BackButton());
        }

        private IEnumerator _BackButton()
        {
            CurrentWorld.EnableLevelsCamera(false);
            m_BackButton.interactable = false;

            yield return new WaitForSeconds(0.5f);

            m_PreviousButton.interactable = CurrentWorld.HasPreviousWorld;
            m_NextButton.interactable = CurrentWorld.HasNextWorld;
        }

        public void NextButton()
        {
            K.SoundManager.PlayFXSound(K.SoundManager.GetSFXByName("click"));
            CurrentWorld.GoToNextWorld();
            CurrentWorld = CurrentWorld.NextWorld;
            m_WorldName.text = CurrentWorld.WorldName;

            m_NextButton.interactable = CurrentWorld.HasNextWorld;
            m_PreviousButton.interactable = CurrentWorld.HasPreviousWorld;
        }

        public void PreviousButton()
        {
            K.SoundManager.PlayFXSound(K.SoundManager.GetSFXByName("click"));
            CurrentWorld.GoToPreviousWorld();
            CurrentWorld = CurrentWorld.PreviousWorld;
            m_WorldName.text = CurrentWorld.WorldName;

            m_PreviousButton.interactable = CurrentWorld.HasPreviousWorld;
            m_NextButton.interactable = CurrentWorld.HasNextWorld;
        }
    }
}
