﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SuperSquad.Challenge.UI
{
    public class MainMenuController : Utils.MonoBehaviour
    {
        [SerializeField]
        private Button m_GenderButton;
        [SerializeField]
        private Button m_ContinueButton;
        [SerializeField]
        private Text m_GenderLabel;
        [SerializeField]
        private Color m_MaleColor;
        [SerializeField]
        private Color m_FemaleColor;
        [SerializeField]
        private Button m_SoundButton;
        [SerializeField]
        private Text m_SoundButtonLabel;

        private void Start()
        {
            if(K.SoundManager.SoundEnabled)
            {
                m_SoundButtonLabel.text = "Audio Off";
                m_SoundButton.onClick.AddListener(() => SoundOff());
            }
            else
            {
                m_SoundButtonLabel.text = "Audio On";
                m_SoundButton.onClick.AddListener(() => SoundOn());
            }
           

            if (K.GameManager.PlayerGender == Common.GameManager.Gender.Male)
            {
                m_GenderLabel.text = "Female";
                m_GenderLabel.color = m_FemaleColor;
                m_GenderButton.onClick.AddListener(() => FemaleButton());

            } 
            else
            {
                m_GenderLabel.text = "Male";
                m_GenderLabel.color = m_MaleColor;
                m_GenderButton.onClick.AddListener(() => MaleButton());
            }

            m_ContinueButton.interactable = K.SaveLoad.LoadedDefaultData;
        }

        public void PlayButton()
        {
            PlayerPrefs.DeleteAll();

            K.SoundManager.PlayFXSound(K.SoundManager.GetSFXByName("click"));
            K.SaveLoad.LoadDefaultData();
            K.EnterWorldSelectionContext();
        }

        public void Continue()
        {
            K.GameManager.CurrentWorld = K.SaveLoad.World;
            K.GameManager.CurrentLevel = K.SaveLoad.Level;

            K.EnterWorldSelectionContext();
        }

        public void MaleButton()
        {
            m_GenderLabel.text = "Female";
            m_GenderLabel.color = m_FemaleColor;
            K.GameManager.PlayerGender = Common.GameManager.Gender.Male;
            K.SaveLoad.SaveCommonData(Kernel.SaveLoad.DataId.Gender, 0);

            m_GenderButton.onClick.RemoveAllListeners();
            m_GenderButton.onClick.AddListener(() => FemaleButton());
        }

        public void FemaleButton()
        {
            m_GenderLabel.text = "Male";
            m_GenderLabel.color = m_MaleColor;
            K.GameManager.PlayerGender = Common.GameManager.Gender.Female;
            K.SaveLoad.SaveCommonData(Kernel.SaveLoad.DataId.Gender, 1);

            m_GenderButton.onClick.RemoveAllListeners();
            m_GenderButton.onClick.AddListener(() => MaleButton());
        }

        public void SoundOn()
        {
            K.SoundManager.PlayFXSound(K.SoundManager.GetSFXByName("click"));
            m_SoundButtonLabel.text = "Audio Off";
            K.SoundManager.ResumeAudioSource();
            K.SaveLoad.SaveCommonData(Kernel.SaveLoad.DataId.Audio, 1);

            m_SoundButton.onClick.RemoveAllListeners();
            m_SoundButton.onClick.AddListener(() => SoundOff());
        }

        public void SoundOff()
        {
            K.SoundManager.PlayFXSound(K.SoundManager.GetSFXByName("click"));
            m_SoundButtonLabel.text = "Audio On";
            K.SoundManager.MuteAudioSource();
            K.SaveLoad.SaveCommonData(Kernel.SaveLoad.DataId.Audio, 0);

            m_SoundButton.onClick.RemoveAllListeners();
            m_SoundButton.onClick.AddListener(() => SoundOn());
        }

        public void Exit()
        {
            Application.Quit();
        }
    }
}
