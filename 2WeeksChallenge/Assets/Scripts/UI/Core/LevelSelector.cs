﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace SuperSquad.Challenge.UI.Core
{
    public class LevelSelector : Utils.MonoBehaviour
    {
        public enum Type { Avaliable, Locked, Complete }

        //[Header("World State")]
        //[SerializeField]
        private Type m_Type;
        public Type WorldType { get { return m_Type; } }

        [Header("Pointer")]
        [SerializeField]
        private GameObject m_Pointer;

        [Header("Materials")]
        [SerializeField]
        private Material m_AvaliableMaterial;
        [SerializeField]
        private Material m_LockedMaterial;
        [SerializeField]
        private Material m_CompleteMaterial;

        [Space]

        [SerializeField]
        private int m_WorldParent;

        [SerializeField]
        private int m_Index;

        void Start()
        {
            bool unlocked = K.SaveLoad.CheckLevelUnlocked(m_WorldParent, m_Index);
            bool completed = K.SaveLoad.CheckLevelCompleted(m_WorldParent, m_Index);

            if (unlocked)
            {
                if (completed)
                {
                    m_Type = Type.Complete;
                }
                else
                {
                    m_Type = Type.Avaliable;
                }
            }
            else
            {
                m_Type = Type.Locked;
            }

            switch (m_Type)
            {
                case Type.Avaliable:
                    this.GetComponent<MeshRenderer>().material = m_AvaliableMaterial;
                    break;

                case Type.Locked:
                    this.GetComponent<MeshRenderer>().material = m_LockedMaterial;
                    break;

                case Type.Complete:
                    this.GetComponent<MeshRenderer>().material = m_CompleteMaterial;
                    break;

            }

            m_Pointer.SetActive(false);
        }

        public void OnPointerEnter()
        {
            this.transform.DOShakeScale(0.5f, 0.5f);
            m_Pointer.SetActive(true);
        }

        public void OnPointerExit()
        {
            m_Pointer.SetActive(false);
        }

        public void OnPointerClick()
        {
            K.SoundManager.PlayFXSound(K.SoundManager.GetSFXByName("click"));

            if (m_Type != Type.Locked)
            {
                m_Pointer.SetActive(false);
                K.GameManager.CurrentLevel = m_Index;
                K.GameManager.CurrentWorld = m_WorldParent;

                if (m_WorldParent == 0)
                    K.EnterFirstWorldContext();
            }
        }
    }
}
