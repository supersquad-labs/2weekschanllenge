﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

namespace SuperSquad.Challenge.UI.Core
{
    public class WorldSelector : Utils.MonoBehaviour
    {
        public enum Type { Avaliable, Locked, Complete }
        public enum State { World, Levels }

        [Header("World Index")]
        [SerializeField]
        private int m_Index;
        public int Index { get { return m_Index; } }

        [Header("World Name")]
        [SerializeField]
        private string m_WorldName;
        public string WorldName { get { return m_WorldName; } }

        private Type m_Type;
        public Type WorldType { get { return m_Type; } }

        [SerializeField]
        private GameObject m_WorldObject;

        [Header("Neighbours")]
        [SerializeField]
        private WorldSelector m_NextWorld;
        public WorldSelector NextWorld { get { return m_NextWorld; } }
        [SerializeField]
        private WorldSelector m_PreviousWorld;
        public WorldSelector PreviousWorld { get { return m_PreviousWorld; } }

        [Header("Levels Root")]
        [SerializeField]
        private GameObject m_LevelsRoot;

        [Header("Virtual Cameras")]
        [SerializeField]
        private GameObject m_WorldCamera;
        [SerializeField]
        private GameObject m_LevelsCamera;

        [SerializeField]
        private GameObject m_CompleteStar;

        [Header("Materials")]
        [SerializeField]
        private Material m_AvaliableMaterial;
        [SerializeField]
        private Material m_LockedMaterial;

        public bool HasNextWorld { get { return m_NextWorld != null; } }
        public bool HasPreviousWorld { get { return m_PreviousWorld != null; } }

        public State CurrentState { get; private set; }

        public bool HasLevels = true;

        // Use this for initialization
        void Start()
        {
            CurrentState = State.World;
            bool unlocked = K.SaveLoad.CheckWordlUnlocked(m_Index);
            bool completed = K.SaveLoad.CheckWorldCompleted(m_Index);

            if (unlocked)
            {
                if (completed)
                {
                    m_Type = Type.Complete;
                }
                else
                {
                    m_Type = Type.Avaliable;
                }
            }
            else
            {
                m_WorldName = "Locked";
                m_Type = Type.Locked;
            }

            switch (m_Type)
            {
                case Type.Avaliable:
                    m_WorldObject.GetComponent<MeshRenderer>().enabled = false;
                    break;

                case Type.Locked:
                    m_WorldObject.GetComponent<MeshRenderer>().enabled = true;
                    m_WorldObject.GetComponent<MeshRenderer>().material = m_LockedMaterial;
                    break;

                case Type.Complete:
                    m_WorldObject.GetComponent<MeshRenderer>().enabled = false;
                    m_WorldObject.SetActive(false);
                    m_CompleteStar.SetActive(true);
                    break;

            } 
        }

        private void Update()
        {
            if (m_WorldObject.activeInHierarchy)
                m_WorldObject.transform.Rotate(new Vector3(0, Time.deltaTime * 50, 0));

            if (m_CompleteStar != null)
                if (m_CompleteStar.activeInHierarchy)
                    m_CompleteStar.transform.Rotate(new Vector3(0, Time.deltaTime * 50, 0));
        }

        public void GoToNextWorld()
        {
            m_WorldCamera.SetActive(false);
            m_NextWorld.EnableWorldCamera();
        }

        public void GoToPreviousWorld()
        {
            m_WorldCamera.SetActive(false);
            m_PreviousWorld.EnableWorldCamera();
        }

        public void EnableWorldCamera()
        {
            m_WorldCamera.SetActive(true);
        }

        public void EnableLevelsCamera(bool enable)
        {
            CurrentState = enable == true ? State.Levels : State.World;
            StartCoroutine(_EnableLevelsCamera(enable));
        }

        private IEnumerator _EnableLevelsCamera(bool enable)
        {
            if(enable)
            {
                m_LevelsCamera.SetActive(true);
                m_LevelsRoot.SetActive(true);
            }
            else
            {
                m_LevelsCamera.SetActive(false);

                yield return new WaitForSeconds(0.5f);

                m_LevelsRoot.SetActive(false);
            }
        }

        public void DoShakeAnimation()
        {
            m_WorldObject.transform.DOShakeScale(0.5f, 0.5f);
        }
    }
}

