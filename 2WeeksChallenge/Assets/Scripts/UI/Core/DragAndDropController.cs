﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SuperSquad.Challenge.Utils;
using SuperSquad.Challenge.Actions;

namespace SuperSquad.Challenge.UI
{
    public class DragAndDropController : Utils.MonoBehaviour
    {
        [Header("Drop Slots")]
        [SerializeField]
        private List<DragAndDropSlot> m_Slots;

        [Header("Drag Slots")]
        [SerializeField]
        private List<CanvasGroup> m_DragSlots;

        public List<Action> GetActionsToExecute()
        {
            List <Action>  actions = new List<Action>();

            for (int i = 0; i < m_Slots.Count; i++)
            {
                if (m_Slots[i].item != null)
                {
                    actions.Add(m_Slots[i].item.GetComponent<Action>());
                }
            }

            return actions;
        }

        public List<DragAndDropSlot> GetSlotsWitAction()
        {
            List<DragAndDropSlot> slots = new List<DragAndDropSlot>();

            for (int i = 0; i < m_Slots.Count; i++)
            {
                if (m_Slots[i].item != null)
                {
                    slots.Add(m_Slots[i]);
                }
            }

            return slots;
        }

        public void EnableDropSlots(bool enable)
        {
            for (int i = 0; i < m_DragSlots.Count; i++)
            {
                m_DragSlots[i].blocksRaycasts = enable;
            }
        }

        public void Restart()
        {
            List<Action> actions = GetActionsToExecute();

            for (int j = 0; j < actions.Count; j++)
            {
                actions[j].NonComplete();
                actions[j].GetComponent<Image>().color = Color.white;
            }

            for (int i = 0; i < m_Slots.Count; i++)
            {
                if (m_Slots[i].item != null)
                {
                    m_Slots[i].item.GetComponent<CanvasGroup>().blocksRaycasts = true;
                    ResourcesPool.Instance.UnloadObject(m_Slots[i].item);
                }
            }
        }

        public void Retry()
        {
            List<Action> actions = GetActionsToExecute();

            for(int j=0; j < actions.Count; j++)
            {
                actions[j].NonComplete();
                actions[j].GetComponent<Image>().color = Color.white;
            }
        }
    }
}